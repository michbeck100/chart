#!/bin/bash

set -eu

source "$(dirname "$0")/utils.sh"

log_with_header "Linting README.md"
helm-docs -c charts -o ../../README.md --log-level warning

if ! git diff --exit-code > /dev/null; then
  warn "README.md is not up to date! 'update-docs' CI job will be executed to update it!"
  exit 1
else
  success "README.md is up to date"
fi
