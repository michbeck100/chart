.cache:
  cache:
    key:
      files:
        - charts/dependabot-gitlab/Chart.lock
    paths:
      - charts/dependabot-gitlab/charts
    policy: pull

.install:
  stage: test
  image: registry.gitlab.com/dependabot-gitlab/ci-images:docker-$DOCKER_VERSION-helm-$HELM_VERSION-kind-0.17-kubectl-1.26
  services:
    - docker:$DOCKER_VERSION-dind
  tags:
    - $LARGE_RUNNER_TAG
  variables:
    DOCKER_HOST: tcp://docker:2375
    NAMESPACE: dependabot
    RELEASE_NAME: dependabot
  parallel:
    matrix:
      - VALUES: [default, secrets, ingress]
  before_script:
    - .gitlab/ci/script/install-kind.sh
    - .gitlab/ci/script/setup-cluster.sh
  script:
    - .gitlab/ci/script/install-app.sh
  after_script:
    - .gitlab/ci/script/log-install.sh

.publish:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:gsutil-5.10
  stage: release
  interruptible: false
  script:
    - .gitlab/ci/script/publish-chart.sh "$RELEASE"

.with-helm-docs:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:helmdocs-1.7

# Build stage
.build-helm-chart:
  stage: build
  image: registry.gitlab.com/dependabot-gitlab/ci-images:helm-$HELM_VERSION
  extends: .cache
  script:
    - .gitlab/ci/script/build-chart.sh
  cache:
    policy: pull-push
  artifacts:
    paths:
      - public/index.yaml
      - dependabot-gitlab-*.tgz

# Static analysis stage
.lint-chart:
  stage: static analysis
  image: registry.gitlab.com/dependabot-gitlab/ci-images:helm-$HELM_VERSION
  extends: .cache
  script:
    - .gitlab/ci/script/lint.sh

.lint-docs:
  extends: .with-helm-docs
  stage: static analysis
  script:
    - .gitlab/ci/script/lint-docs.sh

.kubeconform:
  stage: static analysis
  image: registry.gitlab.com/dependabot-gitlab/ci-images:helm-$HELM_VERSION
  extends: .cache
  parallel:
    matrix:
      - KUBERNETES_VERSION: ['1.22.1', '1.23.1', '1.24.1', '1.25.1']
  script:
    - .gitlab/ci/script/kubeconform.sh

# Test stage
.install-chart:
  extends: .install

.upgrade-chart:
  extends: .install

# Docs stage
.update-docs:
  extends: .with-helm-docs
  stage: docs
  needs: []
  script:
    - .gitlab/ci/script/update-docs.sh

# Release stage
.upload-latest:
  stage: release
  extends: .publish
  variables:
    RELEASE: pre

.publish-pages:
  extends: .publish
  variables:
    RELEASE: stable
  before_script:
    - cp artifacthub-repo.yml public/
  artifacts:
    paths:
      - public

.changelog:
  stage: release
  image: registry.gitlab.com/dependabot-gitlab/ci-images:ruby
  needs:
    - pages
  variables:
    RELEASE_NOTES_FILE: release_notes.md
  script:
    - .gitlab/ci/script/changelog.sh
  interruptible: false
  release:
    tag_name: $CI_COMMIT_TAG
    description: $RELEASE_NOTES_FILE
